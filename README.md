# IB Counter

iBillboard Intreview task ([ibillboard.com/cs/uloha-c-1](http://ibillboard.com/cs/uloha-c-1))

## NPM commands

> npm run

Starts server on port 8080.

> npm dev

(Es)lints sources, watches ./src using nodemon, starts server on port 8080.

> npm test

Run mocha tests.