var QueryDumper = require('../src/lib/dumper');
var MemoryStream = require('memorystream');
var Rx = require('rxjs');
var sinon = require('sinon');

describe("QueryDumper", function () {

    describe("dumpQueries", function () {

        var stream, dumper;

        beforeEach(function () {
            stream = MemoryStream.createWriteStream();
            dumper = new QueryDumper(stream);
        });

        it("should consume requests and dump into a output stream", function (done) {
            var mockRequests = Rx.Observable
                .interval(1)
                .map(function (i) {
                    return {query: {count: i, a: "b"}}
                })
                .take(5);

            var expectedDump =
                '{"count":0,"a":"b"}\n' +
                '{"count":1,"a":"b"}\n' +
                '{"count":2,"a":"b"}\n' +
                '{"count":3,"a":"b"}\n' +
                '{"count":4,"a":"b"}\n';

            dumper.dumpQueries(mockRequests, 10);

            stream.on('finish', function () {
                stream.toString().should.equal(expectedDump);
                done();
            });

        });

        it("should dump periodically by interval setting", function (done) {
            var clock = sinon.useFakeTimers();

            var mockRequests = Rx.Observable
                .interval(150)
                .map(function (i) { return { query: { i: i } } })
                .take(2);

            dumper.dumpQueries(mockRequests, 200);

            stream.toString().should.be.empty;

            clock.tick(200);

            stream.toString().should.equal('{"i":0}\n');

            clock.tick(200);

            stream.toString().should.equal('{"i":0}\n{"i":1}\n');

            clock.restore();
            done();
        })

    })

});
