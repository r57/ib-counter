Counter = require('../src/lib/counter');
var Rx = require('rxjs');
var sinon = require('sinon');


describe("Counter", function () {

    describe("count", function () {

        it("should extract query count param and pass it in a callback", function (done) {

            var mockRequests = Rx.Observable
                .interval(1)
                .map(function () {
                    return {query: {count: 10}}
                })
                .take(5);

            Counter.count(mockRequests, function (count) {
                count.should.equal(50);
                done();
            });

        });

        it("should aggregate during 'interval' and trigger with aggregated count", function (done) {

            var spy = sinon.spy();
            var clock = sinon.useFakeTimers();

            var mockRequests = Rx.Observable
                .interval(30)
                .map(function () {
                    return {query: {count: 10}}
                })
                .take(3);

            Counter.count(mockRequests, spy, 50);

            spy.should.have.callCount(0);

            clock.tick(50);

            spy.should.have.callCount(1);
            spy.getCall(0).args[0].should.equal(10);

            clock.tick(50);

            spy.should.have.callCount(2);
            spy.getCall(1).args[0].should.equal(20);

            clock.restore();
            done();

        })

    })

});