var Rx = require('rxjs');
var _ = require('lodash');

var Counter = {};

/**
 * Extract count params from requests observable and and pass it to a callback
 * @param {Observable} requests Observable of http requests to count upon
 * @param {function} callback Callback that will be called with count to increment with
 * @param {number} [interval] Aggregate counts over this amount of time before triggering callback
 */
Counter.count = function (requests, callback, interval) {
    interval = interval || 500;

    requests

        // extract counts
        .pluck('query', 'count')

        // filter valid count values (numbers)
        .filter(function (count) {
            return !_.isNaN(count);
        })

        // covert to number
        .map(function (count) {
            return +count;
        })

        // conserve resources by calling callback each N millis
        .bufferTime(interval)
        .filter(function (counts) {
            // just non empty buffers
            return counts.length > 0;
        })
        .map(function (counts) {
            // sum back into one count
            return _.sum(counts);
        })

        .subscribe(callback);
};

module.exports = Counter;
