var Rx = require('rxjs');
var _ = require('lodash');
var log = require('bunyan').createLogger({name: 'Dumper'});

/**
 * Consume observables and dumps them into a provided write-able stream
 * @param stream
 */
var QueryDumper = function (stream) {
    this.stream = stream;

    stream.on('error', function (err) {
        log.error('Error in output stream %s', err);
    });
};

/**
 * Consume observable and dumps it
 * @param {Observable} observable Observable of http requests to dump
 * @param {number} [interval] (default is 500)
 */
QueryDumper.prototype.dumpQueries = function (observable, interval) {
    var self = this;

    interval = interval || 500;

    observable

        // unwrap query
        .map(function (req) {
            // dump each query on separate line
            return JSON.stringify(req.query) + '\n';
        })

        // save IO by writing into stream just each N millis
        .bufferTime(interval)
        .filter(function (buffer) {
            // just non empty buffers
            return buffer.length > 0;
        })
        .map(function (chunks) {
            // join back into one chunk
            return _.sum(chunks);
        })

        .subscribe(
            function (chunk) {
                // redirect event into the stream
                log.trace('Dumping %s bytes', chunk.length);
                self.stream.write(chunk);
            }, function (err) {
                // log and clean
                log.error('Error in source observable %s', err);
                self.stream.end();
            }, function () {
                log.info('Source observable completed.');
                // log and clean
                self.stream.end();
            }
        );
};

module.exports = QueryDumper;
