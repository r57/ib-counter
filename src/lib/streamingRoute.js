var Rx = require('rxjs');

module.exports = function (server, method, path) {
    var subject = new Rx.Subject();

    server[method](path, function (req, res, next) {
        subject.next(req);
        res.end();
        return next();
    });

    //returning shared observable so anyone can require and subscribe
    return subject.asObservable().share();
};

