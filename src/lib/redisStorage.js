var YoRedis = require('yoredis');

var log = require('bunyan').createLogger({name: 'Storage'});

/**
 * Abstraction of redis
 * @param {string} connString Connection string
 * @constructor
 */
var Storage = function (connString) {
    this.client = new YoRedis({ url: connString });
};

/**
 * Increment key in redis by value
 * @param key
 * @param value
 * @returns {*}
 */
Storage.prototype.incrementBy = function (key, value) {
    log.trace('Incrementing %s by %s', key, value);
    return this.client.call('incrby', key, value);
};

module.exports = Storage;
