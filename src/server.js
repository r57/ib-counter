var restify = require('restify');
var path = require('path');
var fs = require('fs');
var pkg = require(path.resolve(process.cwd(), 'package.json'));
var log = require('bunyan').createLogger({name: pkg.name});

var Dumper = require('./lib/dumper');
var RedisStorage = require('./lib/redisStorage');
var Counter = require('./lib/counter');
var streamingRoute = require('./lib/streamingRoute');

// kick start server
var server = restify.createServer({
    name: pkg.name,
    version: pkg.version
});
server.use(restify.queryParser());
server.listen(8080, function () {
    log.info('%s listening at %s', server.name, server.url);
});

// get stream of requests
var trackRequests = streamingRoute(server, 'get', '/track');
trackRequests.subscribe(function (req) {
    log.trace("Got query: ", req.query);
});

// dump request queries into a file
var dumpStream = fs.createWriteStream(path.resolve(process.cwd(), 'dump.json'));
var dumper = new Dumper(dumpStream);
dumper.dumpQueries(trackRequests);

// increment redis key
var storage = new RedisStorage('redis://localhost:6379');
Counter.count(
    trackRequests,
    function (count) {
        storage.incrementBy('count', count)
            .then(function (total) {
                log.trace('Count incremented by %s, now %s', count, total);
            }, function (err) {
                log.error('Failed increment count by %s, error: ', count, err);
            });
    }
);
